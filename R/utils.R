#' Find template resource
#'
#' Helper function that returns the file path of a resource file for one of the
#'   package's R Markdown templates.
#'
#' @param template character. Template for which the file is a resource.
#' @param file character. Name of the resource file.
#'
#' @return character. File path of resource file.
#' @export
find_resource <- function(template, file = 'template.tex') {
  res <- system.file(
    "rmarkdown", "templates", template, "resources", file,
    package = "thll"
  )
  if (res == "") stop(
    "Couldn't find template file ", template, "/resources/", file, call. = FALSE
  )
  res
}

#' Install LaTeX dependencies
#'
#' Helper function that installs the TinyTeX base TeX distribution and all
#'   LaTeX packages needed to compile the THLL PDF template using LaTeX.
#'
#' @return NULL
#' @export
install_dependencies <- function() {
  if (!"tinytex" %in% installed.packages()) {
    warning(
      "The TinyTex R package is not installed!\n\n",
      "Please install it by executing `install.packages('tinytex')`\n"
    )
  }

  if (tinytex::tinytex_root() == "") {
    message("Installing TinyTex TeX distribution...\n")
    tinytex::install_tinytex()
  }

  message("Installing THLL PDF template's LaTeX package dependencies...\n")
  tinytex::tlmgr_install(
    paste0(
      "babel-english biber.x86_64-linux biber biblatex biblatex-apa caption ",
      "changepage colortbl csquotes datetime enumitem environ extsizes ",
      "fancyhdr fmtcount footmisc footnotebackref fp fpl hyphenat ifmtarg ",
      "lastpage libertinus-fonts libertinus-otf logreq mathpazo microtype ",
      "ms newfloat palatino pgf preprint psnfss ragged2e setspace ",
      "sourcecodepro symbol tcolorbox textcase textpos titlesec trimspaces ",
      "wrapfig xifthen"
    )
  )
}

#' Alternative text/elements based on output format
#'
#' Returns different text or document elements based on whether the R Markdown
#'   file is rendered to PDF or HTML
#'
#' @param pdf_var character. Text/elements for PDF output.
#' @param html_var character. Text/elements for HTML output.
#'
#' @return Text/elements for the output format the R Markdown file is rendered
#'   to
#' @export
alt <- function(pdf_var, html_var) {
  if (knitr::is_latex_output()) {
    pdf_var
  } else if (knitr::is_html_output()) {
    html_var
  } else {
    "##UNEXPECTED OUTPUT FORMAT##"
  }
}

#' THL colors
#'
#' Helper function to select THL colors by name or by index.
#'
#' @param selection numeric or character vector. Selection of THL color name(s)
#'   or indeces. Defaults to all five colors (red, orange, light green, dark
#'   green, blue).
#'
#' @return character vector. Vector of HEX color definitions for selected THL
#'   colors.
#' @export
thl_colors <- function(selection = c(1:5)) {
  colors <- c(
    red = "#FE3B1F",
    orange = "#FFB600",
    green_light = "#C2D500",
    green_dark = "#009579",
    blue = "#00ABC8",
    black = "#1D252C"
  )

  unname(colors[selection])
}
