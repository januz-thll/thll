
<!-- README.md is generated from README.Rmd. Please edit that file! -->

THLL <img src="man/figures/thll-logo.png" align="right" alt="THLL logo" width="200" style = "border: none; float: right;">
==========================================================================================================================

This R package includes templates and helper functions for publishing
The Humane League Labs (THLL) reports as well as other convenience
functions.

Installation
------------

### R/RStudio

A recent version of [R](https://cloud.r-project.org/) (and optionally
[RStudio](https://rstudio.com/products/rstudio/download/)) should be
installed on the system.

### Pandoc

The template requires a recent version of Pandoc (≥ 2.8). As the version
of Pandoc that is automatically installed with the latest RStudio
version (1.3.1073, as of writing) is lower, one has to install Pandoc
from the project’s [website](https://pandoc.org/installing.html). After
installation, check the pandoc version with

    rmarkdown::pandoc_version()

If the version number does not correspond with the just installed
version, try restarting R, Rstudio, or your computer.

### R package

To install the THLL R package, execute the following commands in an R
session:

    # if the `remotes` package is not already installed
    install.packages("remotes")

    # install package (will also install/update R package dependencies)
    remotes::install_gitlab("humane-league-labs/thll", upgrade = "always")

Restart the R session after installing the R package.

### TeX distribution

To render reports in PDF format, a TeX distribution has to be installed
on the system. The easiest way to install one is by using the `tinytex`
R package which is automatically installed as a dependency when
installing the THLL R package. This approach assumes that no other TeX
distribution is installed on the system so please delete any current
distributions. Then execute the following convenience function which is
included in the R package:

    thll::install_dependencies()

This will

1.  set up the basic `tinytex` TeX distribution.
2.  install all LaTeX packages that are needed to render reports in PDF
    format.

If you prefer using another LaTeX distribution, make sure to install the
following LaTeX packages manually to be able to render reports in PDF
format:

    babel-english       enumitem         ifmtarg           pgf               titlesec
    biber.x86_64-linux  environ          lastpage          preprint          trimspaces
    biber               extsizes         libertinus-fonts  psnfss            wrapfig
    biblatex            fancyhdr         libertinus-otf    ragged2e xifthen
    biblatex-apa        fmtcount         logreq            setspace 
    caption             footmisc         mathpazo          sourcecodepro    
    changepage          footnotebackref  microtype         symbol   
    csquotes            fp               ms                textcase 
    colortbl            fpl              newfloat          tcolorbox    
    datetime            hyphenat         palatino          textpos  

Usage
-----

### R Markdown

#### PDF/HTML output

The easiest way to use the templates is through `RStudio`:

1.  Open a new Markdown file

    <img src="man/figures/rstudio-new-file.png" width="60%" style="display: block; margin: auto;" />

2.  Select “From Template”

3.  Select the THLL report template in PDF or HTML format

4.  Enter a file name and location

    <img src="man/figures/rstudio-template.png" width="70%" style="display: block; margin: auto;" />

An `.Rmd` file with sample text will be opened in the RStudio editor.
Adjust this document as needed. To generate the report, click the

<img src="man/figures/rstudio-knit.png" width="15%" style="display: block; margin: auto;" />

button on top of the editor window. This will create an output file with
the chosen format in the same directory as the `.Rmd` file.

#### Website

The R package also includes a third template to create a very basic HTML
document that can be pasted into THL’s website framework,
`thll::report_website`. This template is best used not in a separate
`.Rmd` file but by adding a second `output` definition to the YAML
header of the `.Rmd` file used to generate PDF or HTML output, e.g.,

    output:
      thll::report_pdf:
        number_sections: true
      thll::report_website:
        default

and then running the following command on the R command line to generate
both output formats:

    rmarkdown::render("~/path/to/file.Rmd", output_format = "all")

### LaTeX

To use the LaTeX template for generating THLL reports in PDF format
directly, i.e., without using R Markdown, one can duplicate the
accompanying [Overleaf
project](https://www.overleaf.com/read/txqjwfkcntmk) or or download the
files to compile them locally.

Documentation
=============

The sample `.Rmd` files that are generated by RStudio as described above
demonstrate how to use different features of the THLL report templates
and give a short introduction into the usage of Pandoc and R Markdown.
Please compare the respective `.Rmd` input file with its rendered output
file to see how the Markdown syntax relates to how the document is
rendered:

**PDF**

-   [:file\_folder: input `.Rmd`
    file](/inst/rmarkdown/templates/report-pdf/skeleton/skeleton.Rmd)
-   [:file\_folder: output `.pdf`
    file](/inst/rmarkdown/templates/report-pdf/resources/skeleton.pdf)

**HTML**

-   [:file\_folder: input `.Rmd`
    file](/inst/rmarkdown/templates/report-html/skeleton/skeleton.Rmd)
-   [:file\_folder: output `.html`
    file](/inst/rmarkdown/templates/report-html/resources/skeleton.html)
