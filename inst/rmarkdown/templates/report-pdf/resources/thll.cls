\NeedsTeXFormat{LaTeX2e}
\AtEndOfClass{\RequirePackage[final]{microtype}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\ProcessOptions*
\LoadClass{extarticle}

% ------------------------------------------------------------------------------
% general
% ------------------------------------------------------------------------------

% language
\RequirePackage[american]{babel}

% graphics
\RequirePackage[final]{graphicx}
\RequirePackage{tikz}

% page layout
\RequirePackage{changepage}
\RequirePackage[
    left=36pt,%
    right=36pt,%
    top=48pt,%
    bottom=60pt,%
    headheight=40pt,%
    headsep=10pt,%
    letterpaper
]{geometry}
\setlength{\columnsep}{24pt}
\setlength{\parindent}{12pt}

% hyphenate verbatim text
\RequirePackage[htt]{hyphenat}

% break URLs at any point
\RequirePackage[hyphens,spaces]{url}
\renewcommand{\UrlBreaks}{%
    \do\/\do\a\do\b\do\c\do\d\do\e\do\f\do\g\do\h\do\i\do\j\do\k\do\l\do\m\do\n%
    \do\o\do\p\do\q\do\r\do\s\do\t\do\u\do\v\do\w\do\x\do\y\do\z\do\A\do\B\do\C%
    \do\D\do\E\do\F\do\G\do\H\do\I\do\J\do\K\do\L\do\M\do\N\do\O\do\P\do\Q\do\R%
    \do\S\do\T\do\U\do\V\do\W\do\X\do\Y\do\Z
}

% box
\RequirePackage[many]{tcolorbox}
\tcbuselibrary{raster}
\RequirePackage{wrapfig}
\RequirePackage{setspace}

% fix bullet lists for pandoc 1.14
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}\setlength{\parskip}{0pt}
}

% lists
\RequirePackage{enumitem}
\setlist[itemize]{
  noitemsep,
  align=parleft,
  labelsep=*,
  partopsep=0mm
}
\setlist[itemize,1]{
%  topsep=1em,
  labelindent=1.5em,
  leftmargin=2.5em,
}
\setlist[itemize,2,3,4,5]{
  topsep=0mm,
  labelindent=1em,
  leftmargin=2em,
}
\setlist[enumerate]{
  labelindent=1.25em,
  leftmargin=2.5em,
  noitemsep,
  labelsep=*,
  partopsep=0mm
}
\setlist[enumerate,2,3,4,5]{
  labelindent=0em,
  leftmargin=2em,
  topsep=0mm,
}

% prevent overfull lines
\setlength{\emergencystretch}{3em}

% quotation environment
\renewenvironment{quote}%
  {\list{}{\leftmargin=2em\rightmargin=2em}\item[]}%
  {\endlist}
  
% quotes
\RequirePackage{csquotes}

% ------------------------------------------------------------------------------
% colors
% ------------------------------------------------------------------------------

% template colors
\RequirePackage{xcolor}
\definecolor{thlblack}{rgb}{0.11372549, 0.145098039, 0.17254902}
\definecolor{thlred}{rgb}{0.996078431, 0.231372549, 0.121568627}
\definecolor{thlorange}{rgb}{1, 0.71372549, 0}
\definecolor{thlblue}{rgb}{0, 0.670588235, 0.784313725}
\definecolor{thlgreen}{rgb}{0.760784314, 0.835294118, 0}
\definecolor{linkcolor}{rgb}{0.949019608, 0.588235294, 0.08627451}
\definecolor{black50}{gray}{0.5}
\definecolor{black30}{gray}{0.7}
\definecolor{color0}{RGB}{0,0,0}
\definecolor{color1}{RGB}{246, 245, 209}
\definecolor{tabledingbat}{RGB}{184,108,108}

% syntax highlighting colors (Pandoc's default color scheme)
\RequirePackage{color}
\RequirePackage{fancyvrb}
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}%,fontsize=\small}
\RequirePackage{framed}
\definecolor{shadecolor}{RGB}{248,248,248}
\newenvironment{Shaded}{\begin{snugshade}}{\end{snugshade}}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{0.94,0.16,0.16}{#1}}
\newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.77,0.63,0.00}{#1}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\BuiltInTok}[1]{#1}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{#1}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ErrorTok}[1]{\textcolor[rgb]{0.64,0.00,0.00}{\textbf{#1}}}
\newcommand{\ExtensionTok}[1]{#1}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ImportTok}[1]{#1}
\newcommand{\InformationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\NormalTok}[1]{#1}
\newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.81,0.36,0.00}{\textbf{#1}}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{#1}}
\newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\RegionMarkerTok}[1]{#1}
\newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\VariableTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\WarningTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}

% ------------------------------------------------------------------------------
% fonts
% ------------------------------------------------------------------------------

% packages
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{bm}
\RequirePackage{mathpazo}
\RequirePackage[scaled]{helvet}
\RequirePackage{pifont}
\RequirePackage[overload]{textcase}
\RequirePackage{fontspec}
\RequirePackage[bold-style=ISO]{unicode-math}

% set font families
\RequirePackage{libertinus-otf}
\setmathfont{Libertinus Math}

\RequirePackage{sourcecodepro}
\setmonofont{Source Code Pro}[Scale=.8]

\setmainfont[
    Ligatures=TeX,
    Numbers={Lining}, % no old style numbers
    UprightFont=*-Regular,
    BoldFont=*-Bold,
    ItalicFont=*-Italic
]{EBGaramond}

\setsansfont[
    Ligatures=TeX,
    UprightFont=*-Regular,
    BoldFont=*-Bold,
    ItalicFont=*-Italic
]{OpenSans}

\newfontfamily{\montserrat}[
    Ligatures=TeX,
    UprightFont=*-SemiBold,
    BoldFont=*-Bold,
    ItalicFont=*-Medium
]{Montserrat}

\newfontfamily{\notoserif}[
    Ligatures=TeX,
    UprightFont=*-Regular,
    BoldFont=*-SemiBold,
    ItalicFont=*-Italic,
    BoldItalicFont=*-SemiBoldItalic
]{NotoSerif}

% set fonts
\newcommand{\titlefont}{%
    \color{thlorange}\montserrat\fontsize{22}{24}\selectfont
}
\newcommand{\subtitlefont}{%
    \color{black30}\montserrat\fontsize{20}{22}\selectfont
}
\newcommand{\reportfont}{%
    \notoserif\itshape\fontsize{14}{14}\selectfont
}
\newcommand{\abstitlefont}{%
    \montserrat\bfseries\linespread{1.2}\fontsize{10}{12}\selectfont
}
\newcommand{\absfont}{%
    \normalfont\sffamily\linespread{1.2}\fontsize{10}{12}\selectfont
}
\newcommand{\keywordstitlefont}{%
    \montserrat\bfseries\linespread{1.2}\fontsize{11}{13}\selectfont
}
\newcommand{\keywordsfont}{%
    \normalfont\sffamily\linespread{1.2}\fontsize{8}{10}\selectfont
}
\newcommand{\secfont}{%
    \montserrat\bfseries\fontsize{12}{14}\selectfont
}
\newcommand{\subsecfont}{%
    \montserrat\bfseries\fontsize{11}{13}\selectfont
}
\newcommand{\subsubsecfont}{%
    \sffamily\bfseries\itshape\fontsize{10}{12}\selectfont
}
\newcommand{\paragraphfont}{%
    \notoserif\bfseries\itshape\fontsize{10}{12}\selectfont
}
\newcommand{\copyrightfont}{%
    \normalfont\sffamily\fontsize{7}{7}\selectfont
}
\newcommand{\footerfont}{%
    \normalfont\sffamily\fontsize{8}{10}\selectfont
}
\newcommand{\tocfont}{%
    \normalfont\sffamily\fontsize{9}{11}\selectfont
}
\renewcommand\UrlFont{\color{linkcolor}}

% euro symbol
\newcommand{\euro}{€}

% ------------------------------------------------------------------------------
% headers/footers
% ------------------------------------------------------------------------------

\RequirePackage{fancyhdr}  % custom headers/footers
\RequirePackage{lastpage}  % Number of pages in the document
\pagestyle{fancy}          % Enables the custom headers/footers

% empty first paee
\fancypagestyle{firststyle}{
   \fancyhead[L,R]{}
   \fancyfoot[L,R]{}
}

% Headers
\fancyhead[L,R]{} % Blank headers for non-title pages.
% Footers
\lfoot{}
\cfoot{}
\rfoot{}
\makeatletter
\newcommand{\printdoifooter}[1]{%
 \ifthenelse{\isundefined{#1}{}}{%
        {\footerfont \thepage}
    }{%
        {\footerfont DOI \href{http://dx.doi.org/#1}{#1}\hspace{1em}|\hspace{1em}\thepage}
    }%
}
\fancyfoot[R]{\printdoifooter{\@doi}}
\makeatother
\fancyfoot[L]{\raisebox{-0.47\height}{\includegraphics[height=2em]{thll-logo}}}
\renewcommand{\headrulewidth}{0pt}% % No header rule
\renewcommand{\footrulewidth}{0pt}% % No footer rule

% ------------------------------------------------------------------------------
% footnotes
% ------------------------------------------------------------------------------

\RequirePackage[flushmargin,ragged]{footmisc}

\newlength{\myFootnoteWidth}%
\newlength{\myFootnoteLabel}%
\setlength{\myFootnoteLabel}{0.5em}
\makeatletter
\renewcommand{\@makefntext}[1]{%
  \setlength{\myFootnoteWidth}{\columnwidth}%
  \addtolength{\myFootnoteWidth}{-\myFootnoteLabel}%
  \noindent\makebox[\myFootnoteLabel][r]{\@makefnmark\ }%
  \parbox[t]{\myFootnoteWidth}{#1}
  }
\makeatother

% ------------------------------------------------------------------------------
% section/subsection/paragraph headings style
% ------------------------------------------------------------------------------

\RequirePackage[explicit]{titlesec}

\titleformat{\section}
  {\secfont}
  {\thesection.}
  {0.5em}
 {\MakeUppercase{#1}}
 %{#1}
  []
\titleformat{\subsection}[block]
  {\subsecfont}
  {\thesubsection.}
  {0.5em}
  {#1}
  []
\titleformat{\subsubsection}[runin]
  {\subsubsecfont}
  {\thesubsubsection.}
  {0.5em}
  {#1}
  []
\titleformat{\paragraph}[runin]
  {\paragraphfont}
  {}
  {0.5em}
  {#1}
\makeatletter
\titlespacing*{\section}{0pt}{3ex \@plus4pt \@minus3pt}{5pt}
\titlespacing*{\subsection}{0pt}{2.5ex \@plus3pt \@minus2pt}{2pt}
\titlespacing*{\subsubsection}{0pt}{2ex \@plus2.5pt \@minus1.5pt}{4pt}
\titlespacing*{\paragraph}{0pt}{1.5ex \@plus2pt \@minus1pt}{4pt}
\makeatother

% ------------------------------------------------------------------------------
% tables/figures
% ------------------------------------------------------------------------------

% table packages
\RequirePackage{colortbl}
\RequirePackage{booktabs}
\RequirePackage{tabularx}

% absolute positioning of figures
\RequirePackage[absolute,overlay]{textpos}
\setlength{\TPHorizModule}{1pt}
\setlength{\TPVertModule}{1pt}
\textblockorigin{0pt}{0pt} % start everything near the top-left corner

% captions
\RequirePackage[
    labelfont={sf, bf, footnotesize},%
    labelsep=space,%
    textfont={sf, footnotesize},
    figurename=Figure,%
    singlelinecheck=off,%
    justification=RaggedRight%
]{caption}

% table captions TODO
% \arrayrulecolor{color0} % Set the color of the horizontal rules
% \renewcommand{\arraystretch}{1.5} % Increased line spacing
% \captionsetup[table]{position=below} % Reduce spacing below caption
% \colorlet{tableheadcolor}{color1} % Set table header colour
% \newcommand{\header}{\rowcolor{tableheadcolor}} % create the command for the header
% \captionsetup[table]{name={{\color{thlorange}\textrm{\ding{110}}}\hspace{0.8em}Table}} % Add the icon to the left of the table name

% ------------------------------------------------------------------------------
% table of contents
% ------------------------------------------------------------------------------

\RequirePackage{titletoc}

\newlength{\tocsep}
\contentsmargin{0cm}
\titlecontents{subsection}
  [2.4\tocsep]
  {\addvspace{3pt}\tocfont}
  {\contentslabel[\thecontentslabel]{1.4\tocsep}}
  {}
  {\ \titlerule*[.5pc]{.}\ \thecontentspage}
  []
\titlecontents{subsubsection}
  [4.3\tocsep]
  {\addvspace{3pt}\tocfont}
  {\contentslabel[\thecontentslabel]{1.9\tocsep}}
  {}
  {\ \titlerule*[.5pc]{.}\ \thecontentspage}
  []

% ------------------------------------------------------------------------------
% citations/bibliography
% ------------------------------------------------------------------------------

\RequirePackage[%
    backend=biber,
    style=apa,
    language=american,
    citestyle=numeric,
    sorting=none,
    autolang=hyphen
]{biblatex}

% language
\DeclareLanguageMapping{american}{american-apa}

% numeric citations, but APA style in the reference section
\DeclareFieldFormat{labelnumberwidth}{\mkbibbrackets{#1}}

\defbibenvironment{bibliography}
    {\list
        {\printtext[labelnumberwidth]{%
         \printfield{labelprefix}%
         \printfield{labelnumber}}}
        {\setlength{\labelwidth}{\labelnumberwidth}%
         \setlength{\leftmargin}{\labelwidth}%
         \setlength{\labelsep}{\biblabelsep}%
         \addtolength{\leftmargin}{\labelsep}%
         \setlength{\itemsep}{\bibitemsep}%
         \setlength{\parsep}{\bibparsep}}%
         \renewcommand*{\makelabel}[1]{\hss##1}}
    {\endlist}
    {\item}

% semicolon as separator for numeric citations
\renewcommand\multicitedelim{\addsemicolon\space}

% full first names in bibliography
\renewbibmacro*{name:apa:family-given}[5]{%
  \ifuseprefix
    {\usebibmacro{name:delim:apa:family-given}{#4#1}%
     \usebibmacro{name:hook}{#4#1}%
     \ifdefvoid{#4}{}{%
       \mkbibnameprefix{#4}\isdot%
       \ifprefchar{}{\bibnamedelimc}}%
     \mkbibnamefamily{#1}\isdot%
     \ifdefvoid{#2}{}{\revsdnamepunct\bibnamedelimd\mkbibnamegiven{#2}\isdot}%
     \ifdefvoid{#5}{}{\addcomma\bibnamedelimd\mkbibnamesuffix{#5}\isdot}}
    {\usebibmacro{name:delim:apa:family-given}{#1}%
     \usebibmacro{name:hook}{#1}%
     \mkbibnamefamily{#1}\isdot
     \ifboolexpe{%
       test {\ifdefvoid{#2}}
       and
       test {\ifdefvoid{#4}}}
       {}
       {\revsdnamepunct}%
     \ifdefvoid{#2}{}{\bibnamedelimd\mkbibnamegiven{#2}\isdot}%
     \ifdefvoid{#4}{}{%
       \bibnamedelimc\mkbibnameprefix{#4}%
       \ifprefchar{}{\bibnamedelimc}}%
\ifdefvoid{#5}{}{\addcomma\bibnamedelimd\mkbibnamesuffix{#5}\isdot}}}

\renewbibmacro*{name:apa:given-family}[5]{%
    \usebibmacro{name:delim}{#2#4#1#5}%
    \usebibmacro{name:hook}{#2#4#1#5}%
    \ifdefvoid{#2}{}{\mkbibnamegiven{#2}\isdot\bibnamedelimd}%
    \ifdefvoid{#4}{}{%
        \mkbibnameprefix{#4}\isdot
        \ifprefchar{}{\bibnamedelimc}}%
    \mkbibnamefamily{#1}\isdot%
    \ifdefvoid{#5}{}{\bibnamedelimd\mkbibnamesuffix{#5}\isdot}
}

% semicolon to separate names in bibliography
\renewcommand*{\multinamedelim}{\addsemicolon\space}

\DeclareDelimFormat[bib,biblist]{finalnamedelim}{%
  \ifthenelse{\value{listcount}>\maxprtauth}
    {}
    {\ifthenelse{\value{liststop}>2}
       {\finalandsemicolon\addspace\&\space}
       {\addspace\&\space}}}

\DeclareDelimFormat[bib,biblist]{finalnamedelim:apa:family-given}{%
  \ifthenelse{\value{listcount}>\maxprtauth}
    {}
    {\ifthenelse{\ifcurrentname{groupauthor}\AND%
                 \value{liststop}=2}
     {\addspace\&\space}
     {\finalandsemicolon\addspace\&\space}}}


% remove month and day for journal articles, books, and chapters
\AtEveryBibitem{%
    \ifboolexpr{%
        test {\ifentrytype{article}} or test {\ifentrytype{book}} or test {\ifentrytype{inbook}}
    }{%
        \clearfield{labelmonth}%
        \clearfield{labelday}
    }{}%
}

% remove urldate if publication date is present
\AtEveryBibitem{%
    \ifboolexpr{not test {\iffieldundef{year}}}{%
        \clearfield{urlyear}
    }
}

% remove the disambiguation markers as we use numerical citations
\DeclareFieldFormat{extradate}{}

% break URLs at any point
\setcounter{biburllcpenalty}{100}
\setcounter{biburlucpenalty}{200}
\setcounter{biburlnumpenalty}{100}

% ------------------------------------------------------------------------------
% author/affiliation
% ------------------------------------------------------------------------------

\RequirePackage{authblk}
\setlength{\affilsep}{0em}
\renewcommand\Authfont{%
    \color{color0}\normalfont\sffamily\bfseries\fontsize{9}{13}\selectfont
}
\renewcommand\Affilfont{
    \color{color0}\normalfont\sffamily\fontsize{8}{10}\selectfont
}
\makeatletter
\renewcommand\AB@authnote[1]{\textsuperscript{#1}}
\renewcommand\AB@affilnote[1]{\textsuperscript{#1}}
\makeatletter
\renewcommand\AB@affilsepx{ | \protect\Affilfont} % one line
\makeatother
\renewcommand\Authands{, and }
\makeatother

% create ORCiD icon with ORCiD number linked to ORCiD
\newcommand{\orcid}[1]{%
    \href{%
        https://orcid.org/#1%
    }{%
        $\vcenter{\hbox{\includegraphics[height=0.16in]{orcid-logo.png}}}$%
    }%
}

\newcommand{\orcidauthor}[3]{%
    \ifthenelse{\isempty{#3}{}}{%
        \author{#1\textsuperscript{#2}}%
    }{%
        \author{#1\textsuperscript{#2}\protect\orcid{#3}}%
    }%
}

\newcommand{\orcidurl}[1]{
    \href{https://orcid.org/#1}{$\vcenter{\hbox{\includegraphics[height=1em]{orcid-logo.png}}}$} \href{https://orcid.org/#1}{https://orcid.org/#1}
}

% ------------------------------------------------------------------------------
% template variables
% ------------------------------------------------------------------------------

% booleans
\RequirePackage{xifthen}
\newboolean{osfdata}
\newboolean{osfmaterials}
\newboolean{osfpreregistered}
\newboolean{osfpreregisteredplus}
\newboolean{equalcontrib}

% meta data fields
\makeatletter
\newcommand{\subtitle}[1]{\def\@subtitle{#1}}
\newcommand{\supertitle}[1]{\def\@supertitle{#1}}
\newcommand{\report}[1]{\def\@report{#1}}
\newcommand{\publicationdate}[1]{\def\@publicationdate{#1}}
\newcommand{\doi}[1]{\def\@doi{#1}}
\newcommand{\osfproject}[1]{\def\@osfproject{#1}}
\newcommand{\osftext}[1]{\def\@osftext{#1}}
\makeatother

% ------------------------------------------------------------------------------
% abstract
% ------------------------------------------------------------------------------

\def\xabstract{abstract}
\long\def\abstract#1\end#2{\def\two{#2}\ifx\two\xabstract
\long\gdef\theabstract{\ignorespaces#1}
\def\go{\end{abstract}}\else
\typeout{^^J^^J PLEASE DO NOT USE ANY \string\begin\space \string\end^^J
COMMANDS WITHIN ABSTRACT^^J^^J}#1\end{#2}
\gdef\theabstract{\vskip12pt BADLY FORMED ABSTRACT: PLEASE DO
NOT USE {\tt\string\begin...\string\end} COMMANDS WITHIN
THE ABSTRACT\vskip12pt}\let\go\relax\fi
\go}

\RequirePackage{ifmtarg}
\RequirePackage{datetime}
\newdateformat{monthdayyeardate}{%
  \textsc{\monthname[\THEMONTH] \THEDAY, \THEYEAR}
}

\makeatletter
\newcommand{\abscontent}{
    \noindent\begin{minipage}{0.9\textwidth}
        {\color{black50}\rule{\linewidth}{0.7pt}}
        %
        % abstract / executive summary
        {\abstitlefont\bfseries\color{thlblack}EXECUTIVE SUMMARY} \absfont\theabstract%
        {\color{black50}\rule{\linewidth}{0.7pt}}
   \end{minipage}%
}%
\makeatother

% ------------------------------------------------------------------------------
% title
% ------------------------------------------------------------------------------

\makeatletter
\renewcommand{\@maketitle}{\bgroup\setlength{\parindent}{0pt}%
    \includegraphics[width=0.5\textwidth]{thll-logo}\\
    \vskip90pt%
    {\hspace{0.75mm}\reportfont \@supertitle\ \@report
    \ifthenelse{\boolean{osfdata}}{\hspace{0.05em} \raisebox{-0.125\height}{\includegraphics[height=1em]{osf-data-small}}}{}%
    \ifthenelse{\boolean{osfmaterials}}{\hspace{0.05em} \raisebox{-0.125\height}{\includegraphics[height=1em]{osf-materials-small}}}{}%
    \ifthenelse{\boolean{osfpreregistered}}{\hspace{0.05em} \raisebox{-0.125\height}{\includegraphics[height=1em]{osf-preregistered-small}}}{}%
    \ifthenelse{\boolean{osfpreregisteredplus}}{\hspace{0.05em} \raisebox{-0.125\height}{\includegraphics[height=1em]{osf-preregisteredplus-small}}}{} %
    \par}%
    \vskip6pt%
    {\hspace{-1mm}
    \begin{minipage}{\textwidth}
        {\titlefont\MakeUppercase{\@title}\par}%
        % subtitle
        \ifthenelse{\isundefined{\@subtitle{}}}{%
            \par
        }{
            {\vskip4pt\subtitlefont\@subtitle{}\par}
        }
    \end{minipage}}
    \vskip10pt
    % publication/revision date
    \ifthenelse{\isundefined{\@publicationdate}}{%
       \hspace{0.35mm}{\keywordstitlefont \monthdayyeardate\today}
    }{%
        \hspace{0.35mm}{\keywordstitlefont\monthdayyeardate\today~(Originally published \monthdayyeardate\@publicationdate)}
    }
    \vskip10pt%
    {\hspace{0.35mm}\@author\par}
    \egroup
    {%
    \vskip14pt%
    \abscontent
    \vspace{10pt}%
    }%
}%
\makeatother

% ------------------------------------------------------------------------------
% OSF info
% ------------------------------------------------------------------------------

\makeatletter
\newcommand{\osfbadgestatement}{
    \ifthenelse{\boolean{osfdata} \AND \boolean{osfmaterials} \AND \boolean{osfpreregisteredplus}}{%
        The research presented in this document displays the \textit{Open Data}, \textit{Open Materials}, and \textit{Preregistered+} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All data, materials, and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfdata} \AND \boolean{osfmaterials} \AND \boolean{osfpreregistered}}{%
        The research presented in this document displays the \textit{Open Data}, \textit{Open Materials}, and \textit{Preregistered} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All data, materials, and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfdata} \AND \boolean{osfmaterials}}{%
        The research presented in this document displays the \textit{Open Data} and \textit{Open Materials} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All data and materials are available in the associated }{%
    %
    \ifthenelse{\boolean{osfdata} \AND \boolean{osfpreregisteredplus}}{%
        The research presented in this document displays the \textit{Open Data} and \textit{Preregistered+} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All data and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfdata} \AND \boolean{osfpreregistered}}{%
        The research presented in this document displays the \textit{Open Data} and \textit{Preregistered} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All data and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfmaterials} \AND \boolean{osfpreregisteredplus}}{%
        The research presented in this document displays the \textit{Open Materials} and \textit{Preregistered+} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All materials and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfmaterials} \AND \boolean{osfpreregistered}}{%
        The research presented in this document displays the \textit{Open Materials} and \textit{Preregistered} \href{https://cos.io/our-services/open-science-badges/}{badges for open science practices}. All materials and the analysis plan are available in the associated }{%
    %
    \ifthenelse{\boolean{osfdata}}{%
        The research presented in this document displays the \textit{Open Data} \href{https://cos.io/our-services/open-science-badges/}{badge for open science practices}. All data are available in the associated }{%
    %
    \ifthenelse{\boolean{osfmaterials}}{%
        The research presented in this document displays the \textit{Open Materials} \href{https://cos.io/our-services/open-science-badges/}{badge for open science practices}. All materials are available in the associated }{%
    %
    \ifthenelse{\boolean{osfpreregisteredplus}}{%
        The research presented in this document displays the \textit{Preregistered+} \href{https://cos.io/our-services/open-science-badges/}{badge for open science practices}. The analysis plan is available in the associated }{%
    %
    \ifthenelse{\boolean{osfpreregistered}}{%
        The research presented in this document displays the \textit{Preregistered} \href{https://cos.io/our-services/open-science-badges/}{badge for open science practices}. The analysis plan is available in the associated }{%
    %
    \ifthenelse{\NOT \boolean{osfdata} \AND \NOT \boolean{osfmaterials} \AND \NOT \boolean{osfpreregistered} \AND \NOT \boolean{osfpreregisteredplus}}{%
        \ifthenelse{\isundefined{\@osftext{}}}{}{\@osftext}
    }{}%
    %
    }}}}}}}}}}}Open Science Framework repository at \href{https://osf.io/\@osfproject/}{https://osf.io/\@osfproject/}.
}
\makeatother

\newcommand{\osfbadgesmessage}{
    \copyrightfont
    % dynamically set box length
    \newsavebox{\osfbadgesbox}
    \sbox{\osfbadgesbox}{
        \hspace{-0.5em}
        \ifthenelse{\boolean{osfdata}}{%
            \hspace{0.1em}\includegraphics[height=3em]{osf-data-large}
        }{}%
        \ifthenelse{\boolean{osfmaterials}}{%
            \hspace{0.1em}\includegraphics[height=3em]{osf-materials-large}
        }{}%
        \ifthenelse{\boolean{osfpreregisteredplus}}{%
            \hspace{0.1em}\includegraphics[height=3em]{osf-preregisteredplus-large}
        }{}%
        \ifthenelse{\boolean{osfpreregistered}}{%
            \hspace{0.1em}\includegraphics[height=3em]{osf-preregistered-large}
        }{}%
        \ifthenelse{\NOT \boolean{osfdata} \AND \NOT \boolean{osfmaterials} \AND \NOT \boolean{osfpreregistered} \AND \NOT \boolean{osfpreregisteredplus}}{%
            \hspace{0.1em}\includegraphics[height=3em]{osf-logo}
        }{}%
        \hspace{0.05em}
    }
    \newlength{\osfbadgesboxlen}
    \settowidth{\osfbadgesboxlen}{\usebox{\osfbadgesbox}}
    % wrap text with wrapfigure
    \setlength{\intextsep}{0pt}%
    \setlength{\columnsep}{0pt}%
    \begin{wrapfigure}{L}{\osfbadgesboxlen}
        \usebox{\osfbadgesbox}
    \end{wrapfigure}
    %
    \osfbadgestatement
}

% ------------------------------------------------------------------------------
% License info
% ------------------------------------------------------------------------------

\newcommand{\copyrightstatement}{%
    This work is licensed under the Creative Commons Attribution 4.0 %
    International License. To view a copy of this license, visit %
    \href{http://creativecommons.org/licenses/by/4.0/}%
    {http://creativecommons.org/licenses/by/4.0/} or send a letter to %
    Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
}

\newcommand{\licensemessage}{
    \copyrightfont
    % dynamically set box length
    \newsavebox{\licensebox}
    \sbox{\licensebox}{
        \includegraphics[height=1.8em]{cc-by}
    }
    \newlength{\licenseboxlen}
    \settowidth{\licenseboxlen}{\usebox{\licensebox}}
    %
    % wrap text with wrapfigure
    \setlength{\intextsep}{0pt}%
    \setlength{\columnsep}{0pt}%
    \begin{wrapfigure}{l}{\licenseboxlen}
        \hspace{0.1em}\includegraphics[height=1.8em]{cc-by}
    \end{wrapfigure}
    %
    \copyrightstatement
}

% ------------------------------------------------------------------------------
% hyperlinks
% ------------------------------------------------------------------------------

\RequirePackage[%
    colorlinks=true,
    allcolors=linkcolor,
    breaklinks=true,
    linktoc=page,
    setpagesize=false,
    unicode=true,
    luatex
]{hyperref}
\RequirePackage{footnotebackref}
\urlstyle{same}

